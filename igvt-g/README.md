カーネルパラメーターに`intel_iommu=on i915.enable_gvt=1 i915.enable_guc=0 `を追加し、再起動

もしくはi915をhttps://wiki.archlinux.org/title/Kernel_mode_setting#Early_KMS_startに指定しているなら

`/etc/modprobe.d/i915.conf`

```bash
options i915 enable_gvt=1 enable_guc=0
```

詳しくはhttps://wiki.archlinux.org/title/Intel_GVT-g

```bash
qemu-img create -f qcow2 name.qcow2 50G
```

```bash
sudo qemu-system-x86_64 \
    -enable-kvm \
    -m 4G \
    -smp cores=2,threads=2,sockets=1,maxcpus=4 \
    -cpu host,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time \
    -machine type=pc,accel=kvm,kernel_irqchip=on \
    -global PIIX4_PM.disable_s3=1 \
    -global PIIX4_PM.disable_s4=1 \
    -name windows-gvt-g-guest \
    -device intel-hda \
    -usb \
    -device usb-tablet \
    -display gtk,gl=on \
    -device vfio-pci,sysfsdev=/sys/devices/pci0000:00/0000:00:02.0/cc46d0b6-9a2d-41b0-ba62-257269cb7877,x-igd-opregion=on,display=on \
    -drive file=./windows.qcow2,format=qcow2,l2-cache-size=8M \
    -cdrom Win10_21H1_Japanese_x64.iso \
    -vga qxl \
```

https://www.reddit.com/r/VFIO/comments/8h352p/guide_running_windows_via_qemukvm_and_intel_gvtg/

VMのWindows側ではintelのグラフィックドライバが必要
